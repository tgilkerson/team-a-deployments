# Team A Deployments

This Gitlab project holds the ArgoCD resources need for Team-A's deployment into different environments.

>Note: The files here are in a Team-A project, they could be bundled/grouped by environment or by cluster or some other organization.  The best approach will depend on the customer's organization. I have chosen by team because is simple and I hold out hope that each team will manage their own deployment resources.

## Deployments

```sh
# Project
kubectl apply -f app-project.yaml

# Apps
kubectl apply -f app.yaml 
```
